(async () => {
  const { mysql, mssql } = require('./db-adapters')()
  const logger = require('./logger')('gvkhrooj')
  const moment = require('moment-jalaali')
  const { POSSIBLE_SYSTEM_ERROR_MINUTES } = require('./config_local')

  const exit = () => {
    logger.info('OPERATION FINISHED!')
    logger.close()
    process.exit(1)
  } 

  try {
    logger.info('reading the cron job log table')
    const lastCronJobLog = await mysql.raw('SELECT * FROM cron_jobs_logs WHERE gvkhrooj_id IS NOT NULL AND log_id IS NULL ORDER BY fired_at  desc limit 1')
    const cronJobLogRow = lastCronJobLog[0][0]
    
    if(!cronJobLogRow) {
      logger.info('NO LOG FOUND!')
      return exit()
    }

    logger.info('reading new gvkhrooj records')

    const gvkhroojs = await mssql.raw(`SELECT * FROM Gvkhrooj WHERE id > ${cronJobLogRow.gvkhrooj_id} ORDER BY id ASC`)
    // const gvkhroojRows = gvkhroojs[0]

    if(gvkhroojs.length === 0) {
      logger.info('NO NEW GVKHROOJ RECORD FOUND!')
      return exit()
    }

    const entryLogsPromises = gvkhroojs.map(async (gvkhrooj) => {
      const { STID: stid, DATE_: d, TIME_: t } = gvkhrooj
      const jDate = `13${d.substring(0,2)}/${d.substring(2,4)}/${d.substring(4)} ${t.substring(0,2)}:${t.substring(2)}`
      const date = moment(jDate, 'jYYYY/jM/jD HH:mm').format('YYYY-M-D HH:mm:ss')
   
      const lastLog = await mysql.raw(`SELECT * FROM professors_entry_logs WHERE stid = ${stid} ORDER BY date DESC LIMIT 1`)
      const lastLogRow = lastLog[0][0]

      const now = moment().format('YYYY-MM-DD HH:mm:ss')
      
      const newEntryDay = moment(date, 'YYYY-M-D HH:mm:ss').format('DD')
      const lastEntryDay = lastLogRow ? moment(lastLogRow.date).format('DD') : -1

      if(!lastLogRow || newEntryDay != lastEntryDay) {
        return mysql('professors_entry_logs').insert({stid, date, action_type: 'arrival', 
          created_at: now, updated_at: now})
      } else if((new Date(date)).valueOf() - (new Date(lastLogRow.date)).valueOf() > (POSSIBLE_SYSTEM_ERROR_MINUTES * 60 * 1000)) {
        return mysql('professors_entry_logs')
          .insert({stid, date, action_type: lastLogRow.action_type === 'arrival' ? 'departure' : 'arrival',
            created_at: now, updated_at: now})        
      }
    })

    logger.info('writing new logs in professors_entry_log table')
    const entryLogs = await Promise.all(entryLogsPromises)

    logger.info('writing fired cron job log in db')
    const biggestGvkhroojId = gvkhroojs.reduce((id, gvkhrooj) => {
      if(gvkhrooj.id > id) id = gvkhrooj.id
      return id
    }, 0)

    const now = moment().format('YYYY-MM-DD HH:mm:ss')
    await mysql('cron_jobs_logs').insert({fired_at: now, gvkhrooj_id: biggestGvkhroojId, created_at: now, updated_at: now})

    logger.info('SUCCESS!')
    exit()
  } catch (err) {
    logger.error(err)
    exit()
  }
})()


