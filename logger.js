const fs = require('fs')
const moment = require('moment')
const { LOG_FOLDER_ABSOLUTE_PATH } = require('./config_local')


module.exports = (appName) => {
  const now = moment()
  let logText = `CRON JOB LOG ${now}\n FOR *** ${appName} ***`
  
  const time = () => (moment().format('HH:mm:ss'))

  const info =  (text) => {
    const t = `${time()} => ${text}`
    console.log(t)
    logText = `${logText}${t}\n`
  }

  const error = (text) => {
    const t = `***ERROR*** ${time()} => ${text}`
    console.error(t)
    logText = `${logText}${t}\n`
  }

  const close = () => {
    console.log('Detailed log could be found in log folder!')
    fs.writeFileSync(`${LOG_FOLDER_ABSOLUTE_PATH}/${appName}_${now.format('YYYY_MM_DD-HH:mm:ss')}.txt`, logText)
  }

  return { info, error, close }
}

