(async () => {
  const { mysql } = require('./db-adapters')()
  const logger = require('./logger')('status_updater')
  const moment = require('moment-jalaali')

  const exit = () => {
    logger.info('OPERATION FINISHED!')
    logger.close()
    process.exit(1)
  } 

  try {
    logger.info('reading the cron job log table')
    const lastCronJobLog = await mysql.raw('SELECT * FROM cron_jobs_logs WHERE gvkhrooj_id IS NULL AND log_id IS NOT NULL ORDER BY fired_at  desc limit 1')
    const cronJobLogRow = lastCronJobLog[0][0]
    
    if(!cronJobLogRow) {
      logger.info('NO LOG FOUND!')
      return exit()
    }

    logger.info('reading new professors entry logs records')

    const entryLogs = await mysql.raw(`SELECT * FROM professors_entry_logs WHERE id > ${cronJobLogRow.log_id} ORDER BY id ASC`)
    const entryLogRows = entryLogs[0]

    if(entryLogRows.length === 0) {
      logger.info('NO NEW PROFESSORS ENTRY LOGS RECORD FOUND!')
      return exit()
    }

    const professorsUpdatePromises = entryLogRows.map(async (entryLog) => {
      const { stid, action_type } = entryLog
      const is_in_university = action_type === 'arrival' ? true : false
      const now = moment().format('YYYY-MM-DD HH:mm:ss')   


      const prof = await mysql('professors').where({stid}).first()
      if(prof)
        return mysql('professors').where({ stid }).update({ is_in_university })
      else
        return mysql('professors').insert({
          stid,
          first_name: 'نام',
          last_name: 'فامیلی',
          label: 'استاد دانشگاه',
          image_url: 'default.jpg',
          gender: 2,
          is_in_university,
          enable: false,
          created_at: now,
          updated_at: now
        })
    })

    logger.info('updating status of professors')
    const professors = await Promise.all(professorsUpdatePromises)

    logger.info('writing fired cron job log in db')
    const biggestProfessorsEntryLogId = entryLogRows.reduce((maxId, professors_log) => {
      if(professors_log.id > maxId) maxId = professors_log.id
      return maxId
    }, 0)

    const now = moment().format('YYYY-MM-DD HH:mm:ss')
    await mysql('cron_jobs_logs').insert({fired_at: now, log_id: biggestProfessorsEntryLogId, created_at: now, updated_at: now})

    logger.info('SUCCESS!')
    exit()
  } catch (err) {
    logger.error(err)
    exit()
  }
})()


