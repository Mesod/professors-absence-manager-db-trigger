(async () => {
  const { mysql, mssql } = require('./db-adapters')()
  const logger = require('./logger')('prof-mirror')
  const moment = require('moment-jalaali')

  const exit = () => {
    logger.info('OPERATION FINISHED!')
    logger.close()
    process.exit(1)
  }

  try {
    const now = moment().format('YYYY-MM-DD HH:mm:ss')   
    
    logger.info('READING FROM STUDENTS')
    const rawProfessors = await mssql('STUDENTS').select('*')
    
    logger.info('REFORMATING PROFESSORS')
    const professors = rawProfessors.map(prof => {
      const { STID, FNAME = 'نام', LNAME = 'فامیلی', SEX = 2 } = prof
      return {
        stid: STID,
        first_name: FNAME ? FNAME : 'نام',
        last_name: LNAME ? LNAME : 'فامیلی',
        label: 'استاد دانشگاه',
        image_url: 'default.jpg',
        gender: SEX ? SEX : 2,
        is_in_university: false,
        enable: true,
        created_at: now,
        updated_at: now
      }
    })
    
    logger.info('INSERTING PROFESSORS')
    const insertProfessorsPromises = professors.map(prof => (mysql('professors').insert(prof)))
    const insertProfessorsResult = await Promise.all(insertProfessorsPromises)

    logger.info('SUCCESS!')
    exit()
  } catch (err) {
    logger.error(err)
    exit()
  }
})()