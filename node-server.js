const express = require('express')
const app = express()
const morgan = require('morgan')
const { PORT, PROF_IMAGES_FOLDER_ABSOLUTE_PATH } = require('./config_local')
const { mysql } = require('./db-adapters')()
const professorsImageSubRoute = '/professors-images'

app.use(morgan('common'))

// serving static files
app.use('/', express.static('./static-professors-vCard/dist'))
app.use(professorsImageSubRoute, express.static(PROF_IMAGES_FOLDER_ABSOLUTE_PATH))

// api call to get active users
app.get('/active-professors', async(req, res) => {
  const activeProfessors = await mysql('professors').where({enable: true, is_in_university: true})
  
  const response = activeProfessors.map(professor => {
    const { first_name, last_name, image_url, label } = professor
    return {
      name: `${first_name} ${last_name}`,
      label, 
      img: `${professorsImageSubRoute}/${image_url}`
    }
  })

  res.status(200).json(response)
  res.end()
})

app.listen(PORT, () => console.log(`Server is listening on ${PORT}!`))