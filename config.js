module.exports = {
  MYSQL_HOST: '127.0.0.1',
  MYSQL_USER: 'root',
  MYSQL_PASS: 'test',
  MYSQL_DATABASE: 'time_tables',
  MSSQL_HOST: '',
  MSSQL_USER: '',
  MSSQL_PASS: '',
  MSSQL_DATABASE: '',
  POSSIBLE_SYSTEM_ERROR_MINUTES: 10,
  PORT: 3000,
  LOG_FOLDER_ABSOLUTE_PATH: '',
  PROF_IMAGES_FOLDER_ABSOLUTE_PATH: '/Users/mesod/Projects/University/professors-absence-manager-db-trigger/professors_images'
}