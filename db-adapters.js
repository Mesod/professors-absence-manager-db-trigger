const knex = require('knex')
const config = require('./config_local')

module.exports = () => {
  const mysql = knex({
    client: 'mysql',
    connection: {
      host : config.MYSQL_HOST,
      user : config.MYSQL_USER,
      password : config.MYSQL_PASS,
      database : config.MYSQL_DATABASE
    }
  })
  
  const mssql = knex({
    client: 'mssql',
    connection: {
      host : config.MSSQL_HOST,
      user : config.MSSQL_USER,
      password : config.MSSQL_PASS,
      database : config.MSSQL_DATABASE
    }
  })
  
  
  // const mssql = knex({
  //   client: 'mysql',
  //   connection: {
  //     host : config.MYSQL_HOST,
  //     user : config.MYSQL_USER,
  //     password : config.MYSQL_PASS,
  //     database : config.MYSQL_DATABASE
  //   }
  // })

  return { mysql, mssql }
}